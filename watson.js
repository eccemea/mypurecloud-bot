const watson = require('watson-developer-cloud');

const API_KEY = 'rkBOPkUQpGz0x1rZI1PEGyjwb9BeHOkB2bSauTjKZdQy';
const URL = 'https://gateway-lon.watsonplatform.net/assistant/api';
const WORKSPACE_ID = '6c405800-e9da-46be-8852-04e07a678e2e';


var myContext; // This is global. For PRD solution, myContext variable should be stored to the user conversation object.

const assistant = new watson.AssistantV1({
    iam_apikey: API_KEY,
    version: '2018-09-20',
    url: URL
});


function sendMessage(_msg, _adapter, _context) {

    try {
        assistant.message({
            workspace_id: WORKSPACE_ID,
            input: { 'text': _msg },
            context: myContext
        }, function (err, response) {
            if (err) {
                console.log('error:', err);
                sendMessageToBot(_adapter, _context, 'Ups, something goes wrong with Watson');
            }
            else {
                console.log(response);
                myContext = response.context;
                sendMessageToBot(_adapter, _context, response.output.text[0]);
            }

        });
    } catch (error) {
        console.log(error);
        sendMessageToBot(_adapter, _context, 'TryCatch - something goes wrong with Watson');
    }

}


// send message to the bot
async function sendMessageToBot(_adapter, _context, _msg) {
    await _adapter.continueConversation(_context, async (proactiveTurnContext) => {
        await proactiveTurnContext.sendActivity(_msg);
    });
}


module.exports = { sendMessage }
